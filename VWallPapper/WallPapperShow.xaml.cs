﻿using System;
using System.Windows;
using System.Windows.Interop;
using WinWallpaper.Utils;
using System.Windows.Forms.Integration;
using System.Windows.Forms;

namespace VWallPapper
{
    /// <summary>
    /// WallPapperShow.xaml 的交互逻辑
    /// </summary>
    public partial class WallPapperShow : Window
    {
        public WallPapperShow()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            Viewer.BackColor = System.Drawing.Color.White;
            Viewer.Navigate("file:///F:/Summer-V.html");
            //Viewer.TabIndex = 0;
            this.Left = 0;
            this.Top = 0;
            this.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
            this.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.None;
            this.ResizeMode = ResizeMode.NoResize;
            this.Topmost = true;
            var handle = new WindowInteropHelper(this).Handle;
            Screen screen = Screen.FromHandle(handle);
            this.MaxWidth = screen.Bounds.Width;
            this.MaxHeight = screen.Bounds.Height;
            this.WindowState = WindowState.Maximized;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            IntPtr thisIntPtr = new WindowInteropHelper(this).Handle;
            WinAPI.Win32API.User32.SetParent(thisIntPtr, WallpaperUtils.GetWorkerW());
            
        }
    }
}
