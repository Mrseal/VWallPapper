﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Windows;
using System.IO;
using System.Xml.Linq;

namespace WinAPI
{
    public static class Config
    {
        private static string config_file_path = @"./config.cfg";
        public static string GetOldConfigAddress()
        {
            string addr = "";
            XmlDocument doc = new XmlDocument();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true;//忽略文档里面的注释
            XElement xElement = new XElement(new XElement("SavePath"));
            try
            {
                XmlReader reader = XmlReader.Create(config_file_path, settings);
                doc.Load(reader);
                XmlNode root = doc.SelectSingleNode("SavePath");
                if (root != null)
                {
                    addr = root.FirstChild.InnerText;
                }
                reader.Close();
            }
            catch(Exception e)
            {
                try
                {
                    //File.Create(config_file_path);
                   
                    XmlWriter xw = XmlWriter.Create(config_file_path);
                    xElement.Save(xw);
                    xw.Flush();
                    xw.Close();
                    GetOldConfigAddress();
                }catch(Exception e2)
                {

                }
            }
            
            return addr;
        }
        public static void SavePath(string path)
        {
            XmlDocument doc = new XmlDocument();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true;//忽略文档里面的注释
            XmlReader reader = XmlReader.Create(config_file_path, settings);
            doc.Load(reader);
            XmlNode root = doc.SelectSingleNode("SavePath");
            XmlElement pam_path = doc.CreateElement("Path");
            pam_path.InnerText = path;
            root.AppendChild(pam_path);
        }

    }
}
